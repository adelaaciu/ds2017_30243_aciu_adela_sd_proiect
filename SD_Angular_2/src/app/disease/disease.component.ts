import {Component, OnInit} from '@angular/core';
import {Disease, DiseaseService} from './disease.service';
import {LoginService} from '../login/login.service';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-disease-component',
  templateUrl: './disease.component.html',
  providers: [DiseaseService, LoginService]
})
export class DiseaseComponent implements OnInit {
  diseaseName: string;
  diseases: Disease[];
  disease: Disease;
  comboBoxDiseaseList: Disease[];
  category;
  symptoms: string;
  diseaseNameForUpdate: string;
  categoryForUpdate: string;
  symptomsForUpdate: string;
  symptomsListForUpdate: string;
  diseaseForUpdate: Disease;
  private diseaseNameForDelete: string;
  symptomStr;
  x: string;
  isAdmin: boolean;

  constructor(private diseaseService: DiseaseService, private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.getDiseases();
    /*   const user = JSON.parse(localStorage.getItem('user'));
       console.log(user);
       if (null != user) {
         this.isAdmin = user.userRole === ('ROLE_ADMIN');
       } else {
         this.isAdmin = false;
       }
   */
    this.isAdmin = this.loginService.isAdmin();
    console.log(this.isAdmin);
  }

  getDiseaseByName() {
    if (null != this.diseaseName) {
      this.diseaseService.getDiseaseByName(this.diseaseName).subscribe(data => {
          this.disease = data;
          console.log(this.diseaseName);
        }
      );
    }
  }

  getDiseaseByCategory(category) {
    console.log(category);
    this.diseaseService.getDiseaseByCategory(category).subscribe(data => {
      this.comboBoxDiseaseList = data;
      console.log(this.comboBoxDiseaseList);
    });
  }

  getDiseases() {
    this.diseaseService.getDiseases().subscribe(diseaseList => {
      this.diseases = diseaseList;
      console.log(diseaseList);
    });
  }

  addDisease() {
    this.diseaseService.addDisease(this.diseaseName, this.category, this.symptomStr).subscribe(data => {
      console.log(data);
    });
  }

  updateDisease() {
    this.diseaseService.updateDisease(this.diseaseName, this.diseaseNameForUpdate, this.categoryForUpdate, this.symptomsListForUpdate).subscribe(data =>
      console.log(data), err => {
      alert('greseala');
    });
  }

  deleteDisease() {
    this.diseaseService.deleteDisease(this.diseaseNameForDelete)
      .subscribe(
      data => {
          console.log(JSON.parse(data).status);
      }
    );
  }

  getDiseaseByNameForUpdate() {
    this.diseaseService.getDiseaseByNameForUpdate(this.diseaseNameForUpdate).subscribe(diseases => {
        this.diseaseName = this.diseaseNameForUpdate;
        this.diseaseForUpdate = diseases;
        this.categoryForUpdate = this.diseaseForUpdate.category;
        this.symptomsListForUpdate = this.diseaseForUpdate.symptoms;
      }
    );
  }
}
