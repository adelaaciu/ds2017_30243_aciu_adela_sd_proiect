import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;
const headers = new Headers({
  'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
  'X-Requested-With': 'XMLHttpRequest',
  'Content-type': 'application/json'
});

@Injectable()
export class DiseaseService {
  constructor(private http: Http) {
  }

  getDiseaseByName(name) {
    return this.http.get(baseUrl + '/diseases/name/' + name).map(res => res.json());
  }

  getDiseaseByCategory(category) {
    return this.http.get(baseUrl + '/diseases/category/' + category).map(res => res.json());
  }

  getDiseases() {
    return this.http.get(baseUrl + '/diseases/list')
      .map(res => res.json());
  }

  addDisease(diseaseName: string, category: string, symptoms: string) {
    const disease: Disease = new Disease();
    disease.name = diseaseName;
    disease.symptoms = symptoms;
    disease.category = category;

    return this.http.post(baseUrl + '/diseases/', JSON.stringify(disease),
      {
        headers: headers
      }
    );
  }

  updateDisease(diseaseCurrentName: string, diseaseName: string, category: string, symptoms: string) {
    const disease: Disease = new Disease();
    disease.name = diseaseName;
    disease.category = category;
    disease.symptoms = symptoms;
    console.log(disease);
    return this.http.put(baseUrl + '/diseases/' + diseaseCurrentName, JSON.stringify(disease),
      {
        headers: headers
      }
    );
  }

  deleteDisease(diseaseName: string) {
    return this.http.delete(baseUrl + '/diseases/' + diseaseName,
      {headers: headers}
    ).map(res => res.json());
  }

  getDiseaseByNameForUpdate(diseaseNameForUpdate: string) {
    return this.http.get(baseUrl + '/diseases/name/' + diseaseNameForUpdate).map(res => res.json());
  }
}

export class Disease {
  name;
  symptoms: string;
  category;
}
