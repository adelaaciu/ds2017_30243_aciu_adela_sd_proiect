import {Component} from '@angular/core';
import {DiseaseService} from '../disease/disease.service';

@Component({
  selector: 'app-dropdown-basic',
  templateUrl: './dropdown.html',
  providers: [DiseaseService]
})

export class DemoDropdownBasicComponent {
  diseases: {};

  constructor(private diseaseService: DiseaseService) {
  }


  getDiseases() {
    console.log('1');
    this.diseaseService.getDiseases().subscribe(diseases => this.diseases = diseases);
  }


  getDiseaseByCategory(category) {
    console.log(category);
    this.diseaseService.getDiseaseByCategory(category).subscribe(diseases => {
      this.diseases = diseases;
      console.log(diseases);
    });
  }
}
