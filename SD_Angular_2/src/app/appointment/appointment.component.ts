import {Component, OnInit} from '@angular/core';
import {Appointment, AppointmentService} from './appointment.service';
import {Router} from '@angular/router';
import {isNull, isUndefined} from 'util';
import {Hospital, HospitalService} from 'app/hospital/hospital.service';
import {Doctor, DoctorService} from '../doctor/doctor.service';
import {Sector} from '../sector/sector';


@Component({
  selector: 'app-appointment-component',
  templateUrl: './appointment.component.html',
  providers: [AppointmentService, HospitalService, DoctorService]
})
export class AppointmentComponent implements OnInit {
  doctorName: string;
  hospital: string;
  category: string;
  hospitalCity;
  date;
  hospitalList: Hospital[];
  doctorsList: Doctor[];
  hospitalSectors: Sector[];
  phoneNumber;


  constructor(private appointmentService: AppointmentService,
              private router: Router,
              private hospitalService: HospitalService,
              private doctorService: DoctorService) {
  }


  ngOnInit(): void {
    if (isUndefined(localStorage.getItem('jwtToken'))
      || isNull(localStorage.getItem('jwtToken'))
      || localStorage.getItem('jwtToken') === 'null') {
      this.router.navigate(['/login']);
    } else {

      this.hospitalService.getAllHospitals().subscribe(data => {
        this.hospitalList = data;
      });
    }
  }

  makeAppointment() {
    const appointment: Appointment = new Appointment();
    appointment.doctorName = this.doctorName;
    appointment.hospitalName = this.hospital;
    appointment.hospitalCity = this.hospitalCity;
    appointment.category = this.category;
    appointment.date = this.date;
    appointment.phoneNo = this.phoneNumber;

    this.appointmentService.makeAppointment(appointment).subscribe(app => {
      console.log(app);
    });
  }

  getData(hospitalName) {
    this.hospital = hospitalName;
    const h = this.hospitalList.filter(value => value.name === hospitalName)[0];
    this.hospitalCity = h.city;
    this.hospitalSectors = h.sectorList;
    console.log(this.hospitalSectors);
  }

  getDoctorsByHospitalAndSector() {
    this.doctorService.getDoctorsByHospitalAndSector(this.hospital, this.hospitalCity, this.category).subscribe(data => {
      this.doctorsList = data;
      console.log(this.doctorsList);
    });
  }
}
