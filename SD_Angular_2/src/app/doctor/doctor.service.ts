import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Sector} from '../sector/sector';
import {Hospital} from '../hospital/hospital.service';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;
const headers = new Headers({
  'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
  'X-Requested-With': 'XMLHttpRequest',
  'Content-type': 'application/json'
});

@Injectable()
export class DoctorService {

  constructor(private http: Http) {
  }

  getDoctorsByHospital(hospitalName, hospitalCity) {
    return this.http.get(baseUrl + '/doctors/' + hospitalName + '/' + hospitalCity).map(res => res.json());
  }

  getAllDoctors() {
    return this.http.get(baseUrl + '/doctors/list').map(res => res.json());
  }

  getDoctorsByHospitalAndSector(hospitalName: string, hospitalCity: any, sectorName: string) {
    return this.http.get(baseUrl + '/doctors/' + hospitalName + '/' + hospitalCity + '/' + sectorName).map(res => res.json());
  }

  addDoctor(name: string, phoneNo: string, startHour: string, endHour: string, hospitalName: Hospital, sectorName: Sector) {
    const doctor = new Doctor();
    doctor.name = name;
    doctor.phoneNo = phoneNo;
    doctor.startHour = startHour;
    doctor.endHour = endHour;
    doctor.hospital = hospitalName;
    doctor.sector = sectorName;
    return this.http.post(baseUrl + '/doctors/', JSON.stringify(doctor),
      {
        headers: headers
      }
    );
  }


  updateDoctor(currentName: string, name: string, phoneNo: string, startHour: string, endHour: string, hospital: Hospital, sector: Sector) {
    const doctor = new Doctor();
    doctor.name = name;
    doctor.phoneNo = phoneNo;
    doctor.startHour = startHour;
    doctor.endHour = endHour;
    doctor.hospital = hospital;
    doctor.sector = sector;
    console.log(doctor);
    return this.http.put(baseUrl + '/doctors/' + name, JSON.stringify(doctor),
      {
        headers: headers
      }
    );
  }


  deleteDoctor(name: string) {
    return this.http.delete(baseUrl + '/doctors/' + name,
      {headers: headers}
    ).map(res => res.json());
  }
}

export class Doctor {
  name;
  phoneNo;
  startHour;
  endHour;
  sector: Sector; // toate sectoarele de la un anumit spital
  hospital: Hospital;
}

