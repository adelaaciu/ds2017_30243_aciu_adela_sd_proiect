import {Component, OnInit} from '@angular/core';
import {Doctor, DoctorService} from './doctor.service';
import {Hospital, HospitalService} from '../hospital/hospital.service';
import {LoginService} from '../login/login.service';
import {Sector} from '../sector/sector';

@Component({
  selector: 'app-doctor-component',
  templateUrl: './doctor.component.html',
  providers: [DoctorService, HospitalService, LoginService],
})
export class DoctorComponent implements OnInit {

  hospitalForSectors;
  hospital: string;
  city: string;
  doctors: Doctor[];
  hospitalList: Hospital[];
  private isAdmin: boolean;
  hospitalName;
  sectorName;
  endHourAddDr = '2';
  startHour;
  name;
  phoneNo;
  sectors: Sector[];
  hospitalAdd: Hospital;
  sectorAdd: Sector;

  hospitalNameForUpdate;
  sectorNameForUpdate;
  endHourForUpdate;
  startHourForUpdate;
  nameForUpdate;
  phoneNoForUpdate;
  doctorCurrentName;
  sectorsForUpdate: Sector[];
  hospitalForUpdate: Hospital;
  sect: Sector;

  constructor(private doctorService: DoctorService,
              private hospitalService: HospitalService,
              private loginService: LoginService) {
  }


  ngOnInit(): void {
    this.hospitalService.getAllHospitals().subscribe(data => {
      this.hospitalList = data;
      this.isAdmin = this.loginService.isAdmin();
    });
  }


  getData(hospitalName) {
    this.hospital = hospitalName;
    const h = this.hospitalList.filter(value => value.name === hospitalName)[0];
    console.log(h);
    this.city = h.city;
    console.log(this.city);
  }

  getDoctorsByHospital() {
    console.log(this.hospital);
    console.log(this.city);
    this.doctorService.getDoctorsByHospital(this.hospital, this.city).subscribe(data => {
      this.doctors = data;
    });
  }

  addDoctor() {

    console.log(this.name,
      this.phoneNo,
      this.startHour,
      this.endHourAddDr,
      this.hospitalName,
      this.sectorName);

    this.hospitalAdd.sectorList.forEach(x => {
      if (x.name === this.sectorName) {
        this.sectorAdd = x;
      }
    });

    this.doctorService.addDoctor(this.name,
      this.phoneNo,
      this.startHour,
      this.endHourAddDr,
      this.hospitalAdd,
      this.sectorAdd).subscribe(data => {
      console.log(data);
    });
  }

  updateDoctor() {

    this.hospitalForUpdate.sectorList.forEach(x => {
      if (x.name === this.sectorNameForUpdate) {
        this.sect = x;
      }
    });
    this.doctorService.updateDoctor(this.doctorCurrentName,
      this.nameForUpdate,
      this.phoneNoForUpdate,
      this.startHourForUpdate,
      this.endHourForUpdate,
      this.hospitalForUpdate,
      this.sect).subscribe(data => {
      console.log(data);
    });
  }

  deleteDoctor() {
    this.doctorService.deleteDoctor(this.name).subscribe();
  }

  getHospitalSectors(value) {
    this.hospitalName = value;
    console.log(this.hospitalName);
    this.hospitalList.forEach(x => {
      if (x.name === this.hospitalName) {
        this.hospitalAdd = x;
        this.sectors = x.sectorList;
      }
    });
  }

  getHospitalSectorsForUpdate(value: String) {
    this.hospitalNameForUpdate = value;
    console.log(this.hospitalNameForUpdate);
    this.hospitalList.forEach(x => {
      if (x.name === this.hospitalNameForUpdate) {
        this.hospitalForUpdate = x;
        this.sectorsForUpdate = x.sectorList;
      }
    });
  }


}
