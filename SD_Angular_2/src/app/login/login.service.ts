import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';


const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class LoginService {
  private admin: boolean;

  constructor(private http: Http) {
  }

  login(username, password) {
    // const headers = new Headers({
    //   'Authorization': 'Btoa ' + btoa(username + ':' + password),
    //   'X-Requested-With' : 'XMLHttpRequest'
    // });
    const data = {
      username: username,
      password: password,
      rememberMe: true
    };
    return this.http.post(baseUrl + '/authenticate', data);
  }

  getLoggedUser() {
    const headers = new Headers({
      'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
      'X-Requested-With': 'XMLHttpRequest',
      'Content-type': 'application/json'
    });

    return this.http.get(baseUrl + '/users/logged', {headers: headers});
  }

  isAdmin() {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    if (null != user) {
      this.admin = user.userRole === ('ROLE_ADMIN');
    } else {
      this.admin = false;
    }

    console.log(this.admin);

    return this.admin;
  }

}
