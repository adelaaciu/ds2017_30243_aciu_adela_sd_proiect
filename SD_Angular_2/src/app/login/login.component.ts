import {Component} from '@angular/core';
import {LoginService} from './login.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styles: [`
    .form {
      width: 500px;
    }
  `],
  providers: [LoginService]
})
export class LoginComponent {
  name: string;
  password: string;

  constructor(private loginService: LoginService, private router: Router) {
  }

  login() {
    this.loginService.login(this.name, this.password).subscribe((data) => {
      localStorage.setItem('jwtToken', JSON.parse(data.text()).id_token);
      this.router.navigate(['/appointments']);
      this.getLoggedUser();
    });
  }

  getLoggedUser() {
    this.loginService.getLoggedUser().subscribe(data => {
      const body = JSON.parse(data.text());
      console.log(body);
      localStorage.setItem('user', JSON.stringify(body));
    });
  }

}
