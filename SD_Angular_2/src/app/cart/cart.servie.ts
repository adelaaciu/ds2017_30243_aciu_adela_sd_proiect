import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Drug} from '../drug/drug.service';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;
const headers = new Headers({
  'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
  'X-Requested-With': 'XMLHttpRequest',
  'Content-type': 'application/json'
});

@Injectable()
export class CartService {
  cart: Cart = new Cart();

  constructor(private http: Http) {
  }

  sendCommand(cart: Cart) {
    return this.http.post(baseUrl + '/cart/create/',
      JSON.stringify(cart),
      {headers: headers});
  }

}

export class Cart {
  drugs: Drug[];
  firstName;
  lastName;
  address;
  phoneNo;
}
