import {Component, OnInit} from '@angular/core';
import {Cart, CartService} from './cart.servie';
import {Drug} from '../drug/drug.service';

@Component({
  selector: 'app-cart-component',
  templateUrl: './cart.component.html',
  providers: [CartService]
})

export class CartComponent implements OnInit {
  cart: Cart;
  firstName;
  lastName;
  address;
  phoneNo;
  x: Drug[];

  constructor(private cartService: CartService) {
  }

  ngOnInit(): void {
    this.x = JSON.parse(window.localStorage.getItem('cart'));
    console.log(this.x);
  }


  sendCommamd() {
    this.cart = new Cart();
    this.cart.firstName = this.firstName;
    this.cart.lastName = this.lastName;
    this.cart.address = this.address;
    this.cart.phoneNo = this.phoneNo;
    this.cart.drugs = this.x;
    console.log(this.cart);
    this.cartService.sendCommand(this.cart).subscribe(data => {
      console.log(data);
      localStorage.clear();
      this.x = [];
    });
  }

  removeFromCart(i) {
    this.x.splice(i, 1);
    localStorage.setItem('cart', JSON.stringify(this.x));
  }
}
