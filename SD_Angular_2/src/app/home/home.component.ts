import {Component} from '@angular/core';

@Component({
  selector: 'app-home-component',
  template: `
    <style>
      .home_img{
        height: 400px;
        width: fit-content;
      }
    </style>
  <img class="home_img"src ="assets/images/home_img.jpg">`,
  providers: []
})

export class HomeComponent {
}
