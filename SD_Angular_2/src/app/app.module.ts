import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import {NavbarComponent} from './navbar/navbar.component';
import {RouterModule, RouterOutlet/*, RouterOutletMap, */, Routes} from '@angular/router';
import {DrugsComponent} from './drug/drug.component';
import {DiseaseComponent} from './disease/disease.component';
import {DoctorComponent} from './doctor/doctor.component';
import {DataTableModule} from 'angular2-datatable';
import {HospitalComponent} from './hospital/hospital.component';
import {LoginComponent} from './login/login.component';
import {AppointmentComponent} from './appointment/appointment.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import {RegisterComponent} from 'app/register/register.component';
import {LogoutComponent} from './logout/logout.component';
import { ComboBoxModule } from 'ng2-combobox';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import {CartComponent} from './cart/cart.component';
import {HomeComponent} from './home/home.component';

const appRoutes: Routes = [
  {path: '#', component: HomeComponent},
  {path: 'drugs', component: DrugsComponent},
  {path: 'diseases', component: DiseaseComponent},
  {path: 'doctors', component: DoctorComponent},
  {path: 'hospitals', component: HospitalComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'cart', component: CartComponent},
  {path: 'appointments', component: AppointmentComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DrugsComponent,
    DiseaseComponent,
    DoctorComponent,
    HospitalComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    AppointmentComponent,
    CartComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    RouterModule.forRoot(appRoutes),
    BsDropdownModule.forRoot(),
    ComboBoxModule,
    NguiDatetimePickerModule
  ],
  // providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
