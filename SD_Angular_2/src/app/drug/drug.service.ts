import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Disease} from '../disease/disease.service';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;
const headers = new Headers({
  'X-Requested-With': 'XMLHttpRequest',
  'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
  'Content-type': 'application/json'
});

@Injectable()
export class DrugService {

  constructor(private http: Http) {
  }

  getDrugs() {
    console.log('getDrugs');
    return this.http.get(baseUrl + '/drugs/list').map(res => res.json());
  }

  getDrugByName(name) {
    return this.http.get(baseUrl + '/drugs/' + name).map(res => res.json());
  }

  getDrugsByCategory(category) {
    console.log(category);
    return this.http.get(baseUrl + '/drugs/category/' + category).map(res => res.json());
  }

  addDrug(name: any, category: any, price: any, composition: any, quantity: any, adverseReaction: string, diseaseForAdd: Disease) {
    const drug = this.createDrug(name, category, price, composition, quantity, adverseReaction, diseaseForAdd);
    return this.http.post(baseUrl + '/drugs/', JSON.stringify(drug),
      {
        headers: headers
      }
    );
  }

  updateDrug(currentName: string, name: any, category: any, price: any, composition: any, quantity: any, adverseReaction: string, disease: Disease) {
    const drug = this.createDrug(name, category, price, composition, quantity, adverseReaction, disease);
    return this.http.put(baseUrl + '/drugs/' + name, JSON.stringify(drug),
      {
        headers: headers
      }
    );
  }

  deleteDrug(name: string) {
    return this.http.delete(baseUrl + '/drugs/' + name, {headers: headers}).map(res => res.json());
  }

  createDrug(name: any, category: any, price: any, composition: any, quantity: any, adverseReaction: string, disease: Disease) {
    const drug = new Drug();
    drug.name = name;
    drug.category = category;
    drug.price = price;
    drug.composition = composition;
    drug.quantity = quantity;
    drug.adverseReactions = adverseReaction;
    drug.diseases = [];
    drug.diseases.push(disease);
    return drug;
  }
}

export class Drug {
  name;
  composition;
  category;
  adverseReactions;
  diseases: Disease[];
  quantity;
  price;
}
