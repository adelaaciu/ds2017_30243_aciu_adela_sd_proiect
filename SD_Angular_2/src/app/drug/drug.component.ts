import {Component, OnInit} from '@angular/core';
import {Drug, DrugService} from './drug.service';
import {CartService} from '../cart/cart.servie';
import {Disease, DiseaseService} from '../disease/disease.service';
import {LoginService} from '../login/login.service';

@Component({
  selector: 'app-drug-component',
  templateUrl: './drug.component.html',
  providers: [DrugService, CartService, LoginService, DiseaseService]
})
export class DrugsComponent implements OnInit {
  drugList: Drug[];
  cartList: Drug[] = [];
  name;
  composition;
  category;
  adverseReaction: string;
  diseases: Disease[];
  quantity;
  price;
  isAdmin: boolean;

  nameForUpdate;
  compositionForUpdate;
  categoryForUpdate;
  adverseReactionForUpdate: string;
  drug: Drug;
  quantityForUpdate;
  priceForUpdate;
  currentName;
  diseaseNameForAdd;
  diseaseForAdd: Disease;

  diseaseList: Disease[];
  private diseaseNameForUpdate: string;
  private diseaseForUpdate: Disease;

  ngOnInit(): void {
    this.drugService.getDrugs().subscribe(drugList => {
      this.drugList = drugList;
      console.log(drugList);
      this.isAdmin = this.loginService.isAdmin();
      if (this.isAdmin) {

        this.diseaseService.getDiseases().subscribe(data => {
          this.diseaseList = data;
          console.log(data);
        });
      }
    });
  }

  constructor(private drugService: DrugService, private  loginService: LoginService, private diseaseService: DiseaseService) {
  }

  addToCart(drugIndex) {
    if (this.drugList[drugIndex].quantity > 0) {
      this.cartList.push(this.drugList[drugIndex]);
      this.drugList[drugIndex].quantity--;
      window.localStorage.setItem('cart', JSON.stringify(this.cartList));
    }
  }

  getDrugsByCategory(category) {
    this.drugService.getDrugsByCategory(category).subscribe(drugList => this.drugList = drugList);
  }

  addDrug() {
    this.diseaseList.forEach(x => {
      if (x.name === this.diseaseNameForAdd) {
        this.diseaseForAdd = x;
      }
    });

    console.log(this.diseaseForAdd);

    this.drugService.addDrug(
      this.name,
      this.category,
      this.price,
      this.composition,
      this.quantity,
      this.adverseReaction,
      this.diseaseForAdd).subscribe(data => {
      console.log(data);
    });
  }

  updateDrug() {
    this.diseaseList.forEach(x => {
      if (x.name === this.diseaseNameForUpdate) {
        this.diseaseForUpdate = x;
      }
    });

    console.log(this.diseaseForUpdate);

    this.drugService.updateDrug(this.currentName,
      this.nameForUpdate,
      this.categoryForUpdate,
      this.priceForUpdate,
      this.compositionForUpdate,
      this.quantityForUpdate,
      this.adverseReactionForUpdate,
      this.diseaseForUpdate).subscribe(data => {
      console.log(data);
    });
  }

  deleteDrug() {
    this.drugService.deleteDrug(this.name).subscribe();
  }

  findByName() {
    this.drugService.getDrugByName(this.nameForUpdate).subscribe(data => {
      this.currentName = this.nameForUpdate;
      this.drug = data;
      this.categoryForUpdate = this.drug.category;
      this.priceForUpdate = this.drug.price;
      this.compositionForUpdate = this.drug.composition;
      this.quantityForUpdate = this.drug.quantity;
      this.adverseReactionForUpdate = this.drug.adverseReactions;
    });
  }


}
