import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Doctor} from '../doctor/doctor.service';
import {Sector} from '../sector/sector';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class HospitalService {

  constructor(private http: Http) {
  }

  getHospitalsByCity(city) {
    return this.http.get(baseUrl + '/hospitals?hospitalCity=' + city).map(res => res.json());
  }

  getAllHospitals() {
    return this.http.get(baseUrl + '/hospitals/list').map(res => res.json());
  }
}

export interface  Hospital {
  name;
  city;
  addres;
  doctorList: Doctor[];
  sectorList: Sector[];
}
