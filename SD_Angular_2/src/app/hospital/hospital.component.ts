import {Component} from '@angular/core';
import {Hospital, HospitalService} from './hospital.service';
@Component({
  selector: 'app-hospital-component',
  templateUrl: './hospital.component.html',
  providers: [HospitalService]
})
export class HospitalComponent {
  city: string;
  hospitals: Hospital[];

  constructor(private hospitalService: HospitalService) {
  }

  getHospitalByCity() {
    this.hospitalService.getHospitalsByCity(this.city).subscribe(
      hospitals => {
        this.hospitals = hospitals;
        console.log(hospitals);
      }
    );
  }

  exportListOfHospitals() {

  }
}
