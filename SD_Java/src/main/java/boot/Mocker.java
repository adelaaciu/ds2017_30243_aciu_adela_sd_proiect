//package boot;
//
//import model.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Component;
//import repository.*;
//import service.api.IDrugService;
//
//import javax.annotation.PostConstruct;
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//
//@Component
//public class Mocker {
//
//
//    final IDrugService medicineService;
//    final DrugRepository drugRepository;
//    final UserRepository userRepository;
//    private DiseaseRepository diseaseRepository;
//    private SectorRepository sectorRepository;
//    private HospitalRepository hospitalRepository;
//    private DoctorRepository doctorRepository;
//    private DrugEntity drugEntity1;
//
//
//    @Autowired
//    public Mocker(IDrugService drugService, DrugRepository drugRepository, UserRepository userRepository, DiseaseRepository diseaseRepository, SectorRepository sectorRepository, HospitalRepository hospitalRepository, DoctorRepository doctorRepository) {
//        this.medicineService = drugService;
//        this.drugRepository = drugRepository;
//        this.userRepository = userRepository;
//        this.diseaseRepository = diseaseRepository;
//        this.sectorRepository = sectorRepository;
//        this.hospitalRepository = hospitalRepository;
//        this.doctorRepository = doctorRepository;
//    }
//
//    @PostConstruct
//    public void init() {
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//
//        UserEntity user = new UserEntity();
//        user.setPassword(passwordEncoder.encode("admin"));
//        user.setUsername("admin");
//        user.setEmail("admin");
//        user.setUserRole("ROLE_ADMIN");
//        userRepository.save(user);
//
//        user = new UserEntity();
//        user.setPassword(passwordEncoder.encode("user"));
//        user.setUsername("user");
//        user.setEmail("user");
//        user.setUserRole("ROLE_USER");
//        userRepository.save(user);
//
//        DiseaseEntity diseaseEntity = new DiseaseEntity();
//        diseaseEntity.setName("disease");
//        diseaseEntity.setCategory("inima");
//        diseaseRepository.save(diseaseEntity);
//
//        DiseaseEntity diseaseEntity1 = new DiseaseEntity();
//        diseaseEntity1.setName("disease1");
//        diseaseEntity1.setCategory("inima1");
//        diseaseRepository.save(diseaseEntity1);
//
//
//        SectorEntity sectorEntity = new SectorEntity();
//        sectorEntity.setName("cardiologie");
//        sectorEntity.setLevel("1");
//        sectorEntity.setChambers(10);
//
//        sectorEntity = sectorRepository.save(sectorEntity);
//
//        Set<SectorEntity> sectors = new HashSet<>();
//        sectors.add(sectorEntity);
//
//
//        HospitalEntity hospitalEntity = new HospitalEntity();
//        hospitalEntity.setName("spital1");
//        hospitalEntity.setCity("Oradea");
//        hospitalEntity.setAddress("Andrei");
//        hospitalEntity.setSectors(sectors);
//        hospitalRepository.save(hospitalEntity);
//
//
//        hospitalEntity = new HospitalEntity();
//        hospitalEntity.setName("spital2");
//        hospitalEntity.setCity("Cluj");
//        hospitalEntity.setAddress("Unirii");
//        hospitalEntity.setSectors(sectors);
//        hospitalRepository.save(hospitalEntity);
//
//
//        sectorEntity.setHospital(hospitalEntity);
//        sectorRepository.save(sectorEntity);
//
//        DoctorEntity doctorEntity = new DoctorEntity();
//        doctorEntity.setName("Dr1");
//        doctorEntity.setHospital(hospitalEntity);
//        doctorEntity.setStartHour("10");
//        doctorEntity.setEndHour("19");
//        doctorEntity.setPhoneNo("0770274086");
//        doctorEntity.setSectorEntity(sectorEntity);
//
//        doctorRepository.save(doctorEntity);
//
//
//        DrugEntity drugEntity = new DrugEntity();
//        drugEntity.setName("d2");
//        drugEntity.setCategory("inima");
//        drugEntity.setComposition("etx");
//        drugEntity.setQuantity(0);
//        drugEntity.getDiseaseEntities().add(diseaseEntity);
//        drugEntity.getDiseaseEntities().add(diseaseEntity1);
//        drugRepository.save(drugEntity);
//
//        diseaseEntity.setDrugEntity(drugEntity);
//        diseaseEntity1.setDrugEntity(drugEntity);
//        diseaseRepository.save(diseaseEntity);
//        diseaseRepository.save(diseaseEntity1);
//
//        drugEntity1 = new DrugEntity();
//        drugEntity1.setName("d1");
//        drugEntity1.setCategory("plamani");
//        drugEntity1.setComposition("pulmonar duo");
//        drugEntity1.setQuantity(1);
//        drugEntity1.getDiseaseEntities().add(diseaseEntity);
//        drugRepository.save(drugEntity1);
//
//        diseaseEntity.setSymptoms("symptom1,symptom2");
//        diseaseEntity1.setSymptoms("symptom1,symptom2");
//        diseaseRepository.save(diseaseEntity);
//        drugEntity.getDiseaseEntities().add(diseaseEntity1);
//        diseaseRepository.save(diseaseEntity1);
//
///*
//        ArrayList<String> adverseReactions = new ArrayList<>();
//        adverseReactions.add("adverseReaction1");
//        adverseReactions.add("adverseReaction2");
//        adverseReactions.add("adverseReaction3");
//*/
//
//        drugEntity.setAdverseReactions("adverseReaction3,adverseReaction1");
//        drugEntity1.setAdverseReactions("adverseReaction3");
//        drugRepository.save(drugEntity1);
//        drugRepository.save(drugEntity);
//
//    }
//}
//
