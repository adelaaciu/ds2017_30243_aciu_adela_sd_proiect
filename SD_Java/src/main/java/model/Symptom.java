package model;

import dto.SymptomDTO;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "symptoms")
public class Symptom extends BaseEntity<SymptomDTO>{

    private String name;

    @Override
    public SymptomDTO toDto() {
        SymptomDTO symptomDtO = new SymptomDTO();
        symptomDtO.setId(this.getId());
        symptomDtO.setName(this.name);
        return symptomDtO;
    }

    @Override
    public void fromDto(SymptomDTO dto) {
        this.name = dto.getName();
        this.id = dto.getId();
    }
}
