package model;

import dto.DiseaseDto;
import dto.DrugDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "drugs")
public class DrugEntity extends BaseEntity<DrugDto> {
    @NotNull
    private String name;
    private String composition;
    private String category;
    private String adverseReactions;
    @OneToMany(mappedBy = "drugEntity", fetch = FetchType.EAGER)
    private List<DiseaseEntity> diseaseEntities = new ArrayList<>();
    private int quantity;
    private int price;

    public DrugEntity() {
    }

    @Override
    public DrugDto toDto() {
        DrugDto dto = new DrugDto();
        dto.setName(name);
        dto.setComposition(composition);
        dto.setCategory(category);
        dto.setAdverseReactions(adverseReactions);
        dto.setDiseases(convertDiseaseToDto());
        dto.setQuantity(quantity);
        dto.setPrice(price);
        return dto;
    }

    @Override
    public void fromDto(DrugDto dto) {
        this.name = dto.getName();
        this.composition = dto.getComposition();
        this.category = dto.getCategory();
        this.adverseReactions = dto.getAdverseReactions();
        this.quantity = dto.getQuantity();
        this.price = dto.getPrice();
    }


    private List<DiseaseDto> convertDiseaseToDto() {
        List<DiseaseDto> dtos = new ArrayList<>();

        diseaseEntities.forEach(x -> dtos.add(x.toDto()));

        return dtos;
    }

    private List<DiseaseEntity> convertToDiseaseEntityList(List<DiseaseDto> diseaseDtos) {
        List<DiseaseEntity> diseaseEntityArrayList = new ArrayList<>();
        if (diseaseDtos != null) {
            diseaseDtos.forEach(x -> {
                DiseaseEntity diseaseDtoConsumer = new DiseaseEntity();
                diseaseDtoConsumer.fromDto(x);
                diseaseEntityArrayList.add(diseaseDtoConsumer);
            });
        }
        return diseaseEntityArrayList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAdverseReactions() {
        return adverseReactions;
    }

    public void setAdverseReactions(String adverseReactions) {
        this.adverseReactions = adverseReactions;
    }

    public List<DiseaseEntity> getDiseaseEntities() {
        return diseaseEntities;
    }

    public void setDiseaseEntities(List<DiseaseEntity> diseaseEntities) {
        this.diseaseEntities = diseaseEntities;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "DrugEntity{" +
                "name='" + name + '\'' +
                ", composition='" + composition + '\'' +
                ", category='" + category + '\'' +
                ", adverseReactions=" + adverseReactions +
                ", diseaseEntities=" + diseaseEntities +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DrugEntity that = (DrugEntity) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (composition != null ? !composition.equals(that.composition) : that.composition != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (adverseReactions != null ? !adverseReactions.equals(that.adverseReactions) : that.adverseReactions != null)
            return false;
        return diseaseEntities != null ? diseaseEntities.equals(that.diseaseEntities) : that.diseaseEntities == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (composition != null ? composition.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (adverseReactions != null ? adverseReactions.hashCode() : 0);
        result = 31 * result + (diseaseEntities != null ? diseaseEntities.hashCode() : 0);
        return result;
    }
}
