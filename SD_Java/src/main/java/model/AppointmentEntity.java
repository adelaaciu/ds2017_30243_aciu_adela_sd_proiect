package model;

import dto.AppointmentDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.ZonedDateTime;


@Getter
@Setter
@Data
@Entity
public class AppointmentEntity extends BaseEntity<AppointmentDto> {

    private String doctorName;
    @ManyToOne
    private HospitalEntity hospital;
    private String category;
    private ZonedDateTime date;
    private int phoneNumber;

    @Override
    public AppointmentDto toDto() {
        AppointmentDto appointmentDto = new AppointmentDto();
        appointmentDto.setCategory(category);
        appointmentDto.setDoctorName(doctorName);
        appointmentDto.setHospitalName(hospital.getName());
        appointmentDto.setHospitalCity(hospital.getCity());
        appointmentDto.setCategory(category);
        appointmentDto.setPhoneNumber(phoneNumber);
        return appointmentDto;
    }

    @Override
    public void fromDto(AppointmentDto dto) {
        this.doctorName = dto.getDoctorName();
        this.category = dto.getCategory();
        this.date = ZonedDateTime.parse(dto.getDate());
        this.phoneNumber = dto.getPhoneNumber();
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
