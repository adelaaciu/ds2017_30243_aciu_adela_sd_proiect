package model;

import dto.DoctorDto;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "doctors")
public class DoctorEntity extends BaseEntity<DoctorDto> {
    //    @NotNull
    private String name;
    private String phoneNo;
//    private String email;
//    @OneToOne

    @OneToOne
    private SectorEntity sectorEntity;
    private String startHour;
    private String endHour;
    @ManyToOne
    private HospitalEntity hospital;

    public DoctorEntity() {
    }


    @Override
    public DoctorDto toDto() {
        DoctorDto dto = new DoctorDto();
        dto.setName(name);
        dto.setEndHour(endHour);
        dto.setPhoneNo(phoneNo);
        dto.setStartHour(startHour);
        dto.setSector(sectorEntity.toDto());
        dto.setHospital(hospital.toDto());

        return dto;
    }

    @Override
    public void fromDto(DoctorDto dto) {
        this.name = dto.getName();
        this.endHour = dto.getEndHour();
        this.phoneNo = dto.getPhoneNo();
        this.endHour = dto.getStartHour();
        this.sectorEntity = new SectorEntity();
        this.sectorEntity.fromDto(dto.getSector());
        this.hospital = new HospitalEntity();
        this.hospital.fromDto(dto.getHospital());
    }

    @Override
    public String toString() {
        return "DoctorEntity{" +
                "name='" + name + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", sectorEntity=" + sectorEntity +
                ", startHour='" + startHour + '\'' +
                ", endHour='" + endHour + '\'' +
                ", hospital=" + hospital +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }

    public SectorEntity getSectorEntity() {
        return sectorEntity;
    }

    public void setSectorEntity(SectorEntity sectorEntity) {
        this.sectorEntity = sectorEntity;
    }
}
