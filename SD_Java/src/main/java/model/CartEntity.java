package model;

import dto.CartDto;
import dto.DrugDto;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "chart")
public class CartEntity extends BaseEntity<CartDto> {
    @OneToMany
    private List<DrugEntity> drugs;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNo;

    public CartEntity() {
    }

    public void fromDto(CartDto dto) {
        this.firstName = dto.getFirstName();
        this.lastName = dto.getLastName();
        this.address = dto.getAddress();
        this.phoneNo = dto.getPhoneNo();
        this.drugs = convertDrugsDtoToDrugsEntity(dto.getDrugs());
    }

    @Override
    public CartDto toDto() {
        CartDto dto = new CartDto();
        dto.setAddress(address);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
        dto.setPhoneNo(phoneNo);
        dto.setDrugs(convertEntitiesToDtos());

        return null;
    }


    private List<DrugEntity> convertDrugsDtoToDrugsEntity(List<DrugDto> drugs) {
        List<DrugEntity> drugList = new ArrayList<>();

        drugs.forEach(x -> {
            DrugEntity drug = new DrugEntity();
            drug.fromDto(x);
            drugList.add(drug);
        });

        return drugList;
    }


    private List<DrugDto> convertEntitiesToDtos() {
        List<DrugDto> drugList = new ArrayList<>();

        drugs.forEach(x -> {
            drugList.add(x.toDto());
        });

        return drugList;
    }

    public List<DrugEntity> getDrugs() {
        return drugs;
    }

    public void setDrugs(List<DrugEntity> drugs) {
        this.drugs = drugs;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString() {
        return "CartEntity{" +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                '}';
    }
}
