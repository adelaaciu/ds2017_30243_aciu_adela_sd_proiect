package model;

import dto.SectorDto;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


@Getter
@Setter
@ToString
@Entity
@Table(name = "sectors")
public class SectorEntity extends BaseEntity<SectorDto> {
    @NotNull
    private String name;
    private String level;
    private int chambers;
    public static int NO_MAX_OF_PERSONS = 50;
//    @OneToMany
//    private List<DoctorEntity> doctors;

    @ManyToOne
    public HospitalEntity hospital;

    public SectorEntity() {
    }

    @Override
    public SectorDto toDto() {
        SectorDto dto = new SectorDto();
        dto.setName(name);
        dto.setChambers(chambers);
        dto.setLevel(level);

        return dto;
    }

    @Override
    public void fromDto(SectorDto dto) {
        this.name = dto.getName();
        this.chambers = dto.getChambers();
        this.level = dto.getLevel();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SectorEntity that = (SectorEntity) o;

        if (chambers != that.chambers) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (level != null ? !level.equals(that.level) : that.level != null) return false;
        return hospital != null ? hospital.equals(that.hospital) : that.hospital == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + chambers;
        return result;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getChambers() {
        return chambers;
    }

    public void setChambers(int chambers) {
        this.chambers = chambers;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }
}
