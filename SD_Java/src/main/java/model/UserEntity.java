package model;

import dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Entity
@Table(name = "users")
public class UserEntity extends BaseEntity<UserDto>{

    @NotNull(message = "User can't be null")
    @Column(unique = true)
    private String username;
    @NotNull(message = "Password can't be null")
    private String password;
    private String email;
    private String userRole;

    @Override
    public UserDto toDto() {
        UserDto dto = new UserDto();
        dto.setUsername(username);
        dto.setPassword(password);
        dto.setUserRole(userRole);
        return dto;
    }

    @Override
    public void fromDto(UserDto dto) {
        this.setUsername(dto.getUsername());
        this.setPassword(dto.getPassword());
        this.userRole = dto.getUserRole();
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
