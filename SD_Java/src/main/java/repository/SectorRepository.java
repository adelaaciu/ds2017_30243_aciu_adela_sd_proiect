package repository;

import model.SectorEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SectorRepository extends PagingAndSortingRepository<SectorEntity, Long> {
    SectorEntity findSectorByName(String name);
}
