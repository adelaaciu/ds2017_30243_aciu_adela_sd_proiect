package repository;

import model.DoctorEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DoctorRepository extends PagingAndSortingRepository<DoctorEntity, Long> {
    DoctorEntity findByName(String name);

    List<DoctorEntity> findAllByHospitalNameAndHospitalCity(String hospitalName, String hospitalCity);

    List<DoctorEntity> findAllByHospitalNameAndHospitalCityAndSectorEntityName(String hospitalName, String hospitalCity, String sectorName);
}
