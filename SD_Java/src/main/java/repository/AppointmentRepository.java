package repository;

import model.AppointmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Repository
public interface AppointmentRepository extends JpaRepository<AppointmentEntity, Long>{

    AppointmentEntity findByDoctorNameAndDate(String doctorName, LocalDateTime date);

}
