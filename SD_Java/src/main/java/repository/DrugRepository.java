
package repository;

import model.DrugEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DrugRepository extends PagingAndSortingRepository<DrugEntity, Long> {
    DrugEntity findByName(String name);
    List<DrugEntity> findByCategory(String category);
    List<DrugEntity> findByNameContaining(String name);
}
