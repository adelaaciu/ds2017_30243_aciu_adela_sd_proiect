package repository;

import model.DiseaseEntity;
import model.DrugEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiseaseRepository extends PagingAndSortingRepository<DiseaseEntity, Long> {
    DiseaseEntity findByName(String name);
    List<DiseaseEntity> findAllByNameContaining(String name);
    List<DiseaseEntity> findAllByCategory(String category);

    DiseaseEntity findByDrugEntity(DrugEntity drugEntity);
}
