package export;

import dto.DoctorDto;
import dto.HospitalDto;
import dto.UserDto;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface Convertor {
    File convert(List<DoctorDto> doctorDtos) throws IOException;
}
