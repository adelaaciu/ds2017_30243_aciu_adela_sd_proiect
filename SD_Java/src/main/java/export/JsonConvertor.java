package export;

import com.google.gson.Gson;
import dto.DoctorDto;

import java.io.*;
import java.util.List;


public class JsonConvertor implements Convertor {
    private static final String PATH_TO_FILE = "C:\\Users\\Adela\\Documents\\CTI IV\\SEM I\\SD\\SD_Proiect\\backend\\Fisiere\\fi.json";

    public File convert(List<DoctorDto> doctorDtos) throws IOException {
        File f = new File(PATH_TO_FILE);


        FileOutputStream fop = new FileOutputStream(f);

        if (!f.exists()) {
            f.createNewFile();
        }

        Gson gson = new Gson();
        String json = gson.toJson(doctorDtos);

        byte[] contentInBytes = json.getBytes();

        fop.write(contentInBytes);
        fop.flush();
        fop.close();

        return f;
    }
}
