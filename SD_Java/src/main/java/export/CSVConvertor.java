package export;

import dto.DoctorDto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class CSVConvertor implements Convertor {

    private static final String PATH_TO_FILE = "C:\\Users\\Adela\\Documents\\CTI III\\SEM II\\PS\\Proiect\\Fisiere\\fi.csv";

    public File convert(List<DoctorDto> doctors) throws IOException {
        File f = new File(PATH_TO_FILE);

        FileOutputStream fop = new FileOutputStream(f);


        byte[] contentInBytes;

        String header = "\"name\",\"startHour\",\"endHour\",\"city\", \"address\"";


        contentInBytes = header.getBytes();
        fop.write(contentInBytes);

        for (DoctorDto p : doctors) {
            String s = (String.format("\"{0}\",\"{1}\",\"{2}\",\"{3}\"",
                    p.getName(),
                    p.getStartHour(),
                    p.getEndHour(),
                    p.getPhoneNo()

            ));

            contentInBytes = s.getBytes();
            fop.write(contentInBytes);
        }


        fop.close();
        return f;
    }
}
