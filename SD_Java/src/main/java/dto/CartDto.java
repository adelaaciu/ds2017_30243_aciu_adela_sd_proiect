package dto;

import java.io.Serializable;
import java.util.List;

public class CartDto  implements Serializable {
    List<DrugDto> drugs;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNo;

    public CartDto() {
    }

    public List<DrugDto> getDrugs() {
        return drugs;
    }

    public void setDrugs(List<DrugDto> drugs) {
        this.drugs = drugs;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString() {
        return "CartDto{" +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                '}';
    }
}
