package dto;

import export.Convertor;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Context {
    private  final Convertor convertor;

    public Context(Convertor convertor) {
        this.convertor = convertor;
    }

    public File convertToFormat(List<DoctorDto> doctors) throws IOException {
        return this.convertor.convert(doctors);
    }
}
