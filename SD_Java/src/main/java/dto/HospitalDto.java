package dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Data
public class HospitalDto  implements Serializable {
    private String name;
    private String city;
    private String address;
    private Set<DoctorDto> doctorList;
    private Set<SectorDto> sectorList;

    public HospitalDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<DoctorDto> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(Set<DoctorDto> doctorList) {
        this.doctorList = doctorList;
    }

    public Set<SectorDto> getSectorList() {
        return sectorList;
    }

    public void setSectorList(Set<SectorDto> sectorList) {
        this.sectorList = sectorList;
    }
}
