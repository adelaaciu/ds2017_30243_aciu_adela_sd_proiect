package dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class DiseaseDto  implements Serializable {

    private String name;
    private String category;
    private String symptoms;

    public DiseaseDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }
}
