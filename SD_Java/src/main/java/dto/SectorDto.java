package dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SectorDto  implements Serializable {
    private String name;
    private String level;
    private int chambers;
    public static int NO_MAX_OF_PERSONS = 50;
   // private HospitalDto hospital;

    public SectorDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getChambers() {
        return chambers;
    }

    public void setChambers(int chambers) {
        this.chambers = chambers;
    }

    public static int getNoMaxOfPersons() {
        return NO_MAX_OF_PERSONS;
    }

    public static void setNoMaxOfPersons(int noMaxOfPersons) {
        NO_MAX_OF_PERSONS = noMaxOfPersons;
    }
}
