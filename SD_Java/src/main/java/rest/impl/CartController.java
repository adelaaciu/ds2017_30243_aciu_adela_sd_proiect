package rest.impl;

import dto.CartDto;
import model.CartEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import rest.api.CartEndpoint;
import service.api.ICartService;

import java.net.URI;
@RestController
public class CartController implements CartEndpoint {
    @Autowired
    private ICartService cartService;

    @Override
    public ResponseEntity postCart(@RequestBody CartDto cartDto) {
        CartEntity cart = cartService.postCart(cartDto);
        System.out.println(cart);
        return ResponseEntity.created(URI.create("/" + cart.getId())).body(cart.toDto());
    }
}
