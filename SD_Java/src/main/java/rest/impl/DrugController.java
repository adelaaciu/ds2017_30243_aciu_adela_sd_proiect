package rest.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.DiseaseDto;
import dto.DrugDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import rest.api.DrugEndpoint;
import service.api.IDrugService;

import java.util.List;


@RestController
@CrossOrigin
public class DrugController implements DrugEndpoint {
    @Autowired
    private IDrugService medicineService;


    public ResponseEntity<DrugDto> getMedicineByName(@PathVariable("name") String name) {
        DrugDto dto = medicineService.getDrug(name);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity getMedicineByCategory(@PathVariable("category")String category) {
        List<DrugDto> dto = medicineService.getMedicineByCategory(category);
        if (dto.size() != 0) {
            return ResponseEntity.ok(dto);
        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<DrugDto> getMedicineByDisease(@PathVariable("name") String name) {
        DrugDto dto = medicineService.getMedicineByDisease(name);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<List<DrugDto>> getAllMedicines() {
        List<DrugDto> dtos = medicineService.getAllDrugs();

        if (dtos.size() > 0)
            return ResponseEntity.ok(dtos);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<DrugDto> addMedicine(@RequestBody DrugDto dto) {

        DrugDto drugDto = medicineService.addDrug(dto);

        if (drugDto == null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    public ResponseEntity<DrugDto> updateMedicine(@PathVariable("name") String name, @RequestBody DrugDto dto) {
        DrugDto drugDto = medicineService.updateDrug(name, dto);

        if (drugDto != null) {
            return ResponseEntity.ok(drugDto);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    public ResponseEntity deleteMedicine(@PathVariable("name") String name) {
        if (medicineService.canDelete(name)) {
            medicineService.deleteMedicine(name);
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
