package rest.impl;

import dto.DiseaseDto;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import repository.UserRepository;
import rest.api.DiseaseEndpoint;
import service.api.IDiseaseService;

import java.util.List;


@RestController
public class DiseaseController implements DiseaseEndpoint {
    @Autowired
    private IDiseaseService diseaseService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserController userController;

    public ResponseEntity<DiseaseDto> getDiseaseById(@PathVariable("id") long id) {
        DiseaseDto dto = diseaseService.getDisease(id);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<DiseaseDto> getDiseaseByName(@PathVariable("name") String name) {
        DiseaseDto dto = diseaseService.getDiseaseByName(name);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity getDiseaseByNameContaining(@RequestParam("name") String name) {
        List<DiseaseDto> allByNameContaining = diseaseService.findAllByNameContaining(name);

        if (allByNameContaining != null)
            return ResponseEntity.ok(allByNameContaining);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity getDiseaseByCategory(@PathVariable("category") String category) {
        List<DiseaseDto> diseaseByCategory = diseaseService.getDiseaseByCategory(category);

        if (diseaseByCategory != null)
            return ResponseEntity.ok(diseaseByCategory);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    public ResponseEntity<List<DiseaseDto>> getAllDiseases() {
        List<DiseaseDto> dtos = diseaseService.getAllDiseases();

        if (dtos != null)
            return ResponseEntity.ok(dtos);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<DiseaseDto> addDisease(@RequestBody DiseaseDto dto) {
        DiseaseDto diseaseDto = diseaseService.addDisease(dto);

        if (diseaseDto == null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<DiseaseDto> updateDisease(@PathVariable("name") String name, @RequestBody DiseaseDto dto) {
        DiseaseDto diseaseDto = diseaseService.updateDisease(name, dto);
        if (diseaseDto != null) {
            return ResponseEntity.ok(diseaseDto);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteDisease(@PathVariable("name") String name) {
        if (diseaseService.canDelete(name)) {
            diseaseService.deleteDisease(name);
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public boolean isAdmin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        UserEntity user = userRepository.findByUsername(name);
        return user.getUserRole().equals("ROLE_ADMIN");
    }

}
