package rest.impl;

import dto.UserDto;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import repository.UserRepository;
import rest.api.UserEndpoint;
import service.api.IUserService;

import java.net.URI;


@RestController
@CrossOrigin
public class UserController implements UserEndpoint {
    final IUserService userService;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity create(@RequestBody UserDto userDto) {
        UserEntity user = new UserEntity();
        user.fromDto(userDto);
        UserDto save = userService.save(user);
        return ResponseEntity.created(URI.create("/" + save.getUsername())).body(save);
    }

    @Override
    public ResponseEntity getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @Override
    public ResponseEntity getCurrentLoggedUser(){
        UserEntity loggedUser = getUserEntity();

        return ResponseEntity.ok(loggedUser.toDto());
    }

    public UserEntity getUserEntity() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username

        return userRepository.findByUsername(name);
    }

}
