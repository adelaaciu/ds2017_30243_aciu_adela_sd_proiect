package rest.api;

import dto.AppointmentDto;
import dto.CartDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/cart")
@CrossOrigin
public interface CartEndpoint {
    @PostMapping("/create/")
    ResponseEntity postCart(CartDto appointmentDto);
}
