package rest.api;

import dto.SectorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/sectors")
@CrossOrigin
public interface SectorEndpoint {
    @GetMapping(value = "/{id}")
    ResponseEntity getSector(long id);

    @GetMapping(value = "/list")
    ResponseEntity getAllSectors();

    @PostMapping(value = "/")
    ResponseEntity addSector(SectorDto dto);

    @PutMapping(value = "/{id}")
    ResponseEntity updateSector(long id, SectorDto dto);

    @DeleteMapping(value = "/{id}")
    ResponseEntity deleteSector(long id);
    
}
