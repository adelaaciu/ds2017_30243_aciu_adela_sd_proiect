package rest.api;

import dto.UserDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/users")
public interface UserEndpoint {

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/create")
    ResponseEntity create(@RequestBody UserDto userDto);

    @GetMapping(path = "/list")
    ResponseEntity getAll();

    @GetMapping("/logged")
    ResponseEntity getCurrentLoggedUser();
}
