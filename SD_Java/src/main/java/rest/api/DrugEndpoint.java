package rest.api;

import dto.DrugDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/drugs")
@CrossOrigin
public interface DrugEndpoint {

    @GetMapping(value = "/{name}")
    ResponseEntity getMedicineByName(String name);

    @GetMapping(value = "/category/{category}")
    ResponseEntity getMedicineByCategory(String category);

    @GetMapping(value = "/disease/{name}")
    ResponseEntity getMedicineByDisease(String name);

    @GetMapping(value = "/list")
    ResponseEntity getAllMedicines();

    @PostMapping(value = "/")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity addMedicine(DrugDto dto);

    @PutMapping(value = "/{name}")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity updateMedicine(String name, DrugDto dto);

    @DeleteMapping(value = "/{name}")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity deleteMedicine(String name);
}
