package rest.api;

import dto.DoctorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/doctors")
@CrossOrigin
public interface DoctorEndpoint {
    @GetMapping(value = "/{id}")
    ResponseEntity getDoctor(long id);

    @GetMapping(value = "/{hospitalName}/{hospitalCity}")
    ResponseEntity getDoctors(String hospitalName,String hospitalCity);

    @GetMapping(value = "/{hospitalName}/{hospitalCity}/{sectorName}")
    ResponseEntity getDoctorsForAppointment(String hospitalName,String hospitalCity,String sectorName);

    @GetMapping(value = "/list")
    ResponseEntity getAllDoctors();

    @PostMapping(value = "/")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity addDoctor(DoctorDto dto);

    @PutMapping(value = "/{name}")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity updateDoctor(String name, DoctorDto dto);

    @DeleteMapping(value = "/{name}")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity deleteDoctor(String name);
}
