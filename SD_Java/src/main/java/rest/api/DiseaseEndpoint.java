package rest.api;

import dto.DiseaseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/diseases")
public interface DiseaseEndpoint {


    @GetMapping(value = "/{id}")
    ResponseEntity getDiseaseById(long id);

    @GetMapping(value = "/name/{name}")
    ResponseEntity getDiseaseByName(String name);

    @GetMapping
    ResponseEntity getDiseaseByNameContaining(String name);

    @GetMapping(value = "/category/{category}")
    ResponseEntity getDiseaseByCategory(String category);


    @GetMapping(value = "/list")
    ResponseEntity getAllDiseases();

    @PostMapping(value = "/")
    ResponseEntity addDisease(DiseaseDto dto);

    @PutMapping(value = "/{name}")
    ResponseEntity updateDisease(String name, DiseaseDto dto);

    @DeleteMapping(value = "/{name}")
    ResponseEntity deleteDisease(String name);
}
