package service.api;

import dto.SectorDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ISectorService {
    SectorDto getSector(Long id);

    List<SectorDto> getAllSectors();

    SectorDto addSector(SectorDto dto);

    SectorDto updateSector(Long id, SectorDto dto);

    void deleteSector(Long id);

    boolean canDelete(long id);
}
