package service.api;

import dto.DiseaseDto;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface IDiseaseService {
    DiseaseDto getDisease(Long id);

    List<DiseaseDto> getAllDiseases();

    DiseaseDto addDisease(DiseaseDto dto);

    DiseaseDto updateDisease(String name, DiseaseDto dto);

    void deleteDisease(String name);

    boolean canDelete(String name);

    DiseaseDto getDiseaseByName(String diseaseName);

    List<DiseaseDto> findAllByNameContaining(String name);

    List<DiseaseDto> getDiseaseByCategory(String category);
//    List<DiseaseDto> getDiseaseByMedicine(String medicineName);
}
