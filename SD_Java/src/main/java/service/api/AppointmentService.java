package service.api;

import dto.AppointmentDto;
import model.AppointmentEntity;
import org.springframework.stereotype.Service;


@Service
public interface AppointmentService {

    AppointmentEntity getAppointment(long id);

    AppointmentEntity addAppointment(AppointmentDto appointmentDto);

}
