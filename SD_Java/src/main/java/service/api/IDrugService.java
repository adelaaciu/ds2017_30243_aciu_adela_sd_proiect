package service.api;

import dto.DrugDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IDrugService {
    DrugDto getDrug(Long id);

    List<DrugDto> getAllDrugs();

    DrugDto addDrug(DrugDto dto);

    DrugDto updateDrug(String name, DrugDto dto);

    void deleteMedicine(String name);

    DrugDto getDrug(String name);

    DrugDto getMedicineByDisease(String name);

    boolean canDelete(String name);

    List<DrugDto> getMedicineByCategory(String category);
}
