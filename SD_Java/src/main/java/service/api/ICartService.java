package service.api;

import dto.CartDto;
import model.CartEntity;
import org.springframework.stereotype.Service;

@Service
public interface ICartService {
    CartEntity postCart(CartDto cartDto);
}
