package service.api;

import dto.DoctorDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IDoctorService {
    DoctorDto getDoctor(Long id);

    List<DoctorDto> getAllDoctors();

    DoctorDto addDoctor(DoctorDto dto);

    DoctorDto updateDoctor(String  name, DoctorDto dto);

    void deleteDoctor(String name);

    boolean canDelete(String name);

    List<DoctorDto> getDoctors(String name, String city);

    List<DoctorDto> getDoctorsForAppointment(String name, String city, String sector);
}
