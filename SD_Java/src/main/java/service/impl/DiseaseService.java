package service.impl;

import dto.DiseaseDto;
import model.DiseaseEntity;
import model.Symptom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DiseaseRepository;
import service.api.IDiseaseService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class DiseaseService implements IDiseaseService {
    @Autowired
    private DiseaseRepository diseaseRepository;

    public DiseaseDto getDisease(Long id) {
        DiseaseEntity diseaseEntity = diseaseRepository.findOne(id);
        return diseaseEntity != null ? diseaseEntity.toDto() : null;
    }

    public DiseaseDto getDiseaseByName(String diseaseName) {
        DiseaseEntity diseaseEntity = diseaseRepository.findByName(diseaseName);
        return diseaseEntity != null ? diseaseEntity.toDto() : null;
    }

    public List<DiseaseDto> findAllByNameContaining(String name) {
        List<DiseaseEntity> allByNameContaining = diseaseRepository.findAllByNameContaining(name);
        return allByNameContaining.size() != 0 ?
                convertEntityListToDtoList(allByNameContaining) : null;
    }

    @Override
    public List<DiseaseDto> getDiseaseByCategory(String category) {
        List<DiseaseEntity> allByCategory = diseaseRepository.findAllByCategory(category);
        return allByCategory.size() != 0 ?
                convertEntityListToDtoList(allByCategory) : null;
    }

    public List<DiseaseDto> getAllDiseases() {
        List<DiseaseEntity> diseaseEntityList = (List<DiseaseEntity>) diseaseRepository.findAll();
        return convertEntityListToDtoList(diseaseEntityList);
    }

    private List<DiseaseDto> convertEntityListToDtoList(List<DiseaseEntity> diseaseEntityList) {
        List<DiseaseDto> dtos;

        if (diseaseEntityList.size() != 0) {
            dtos = new ArrayList<>();
            diseaseEntityList.forEach(x -> dtos.add(x.toDto()));

            return dtos;
        }
        return null;
    }

    public DiseaseDto addDisease(DiseaseDto dto) {
        if (diseaseRepository.findByName(dto.getName()) != null) return null;
        DiseaseEntity entity = new DiseaseEntity();
        entity.fromDto(dto);
        entity = diseaseRepository.save(entity);

        return entity.toDto();
    }

    public DiseaseDto updateDisease(String name, DiseaseDto dto) {
        DiseaseEntity entity = diseaseRepository.findByName(name);

        if (entity != null) {
//            ArrayList<DrugEntity> medicineEntityList = getMedicineEntityList(dto);
            // entity.setMedicineEntityList(medicineEntityList);
            entity.setName(dto.getName());
            entity.setSymptoms(dto.getSymptoms());
            entity.setId(entity.getId());
            diseaseRepository.save(entity);

            return dto;
        }

        return null;
    }
//
//    private ArrayList<DrugEntity> getMedicineEntityList(DiseaseDto dto) {
//        ArrayList<DrugEntity> medicineEntityList = new ArrayList<>();
//
//        dto.getMedicineList().forEach(x -> {
//
//            DrugEntity medicineEntity = new DrugEntity();
//            medicineEntity.fromDto(x);
//            medicineEntityList.add(medicineEntity);
//
//        });
//        return medicineEntityList;
//    }

    public void deleteDisease(String name) {
        DiseaseEntity entity = diseaseRepository.findByName(name);
        diseaseRepository.delete(entity);
    }

    public boolean canDelete(String name) {
       DiseaseDto dto  = getDiseaseByName(name);
        return (dto != null);
    }

}
