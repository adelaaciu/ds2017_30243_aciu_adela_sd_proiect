package service.impl;

import dto.UserDto;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import repository.UserRepository;
import service.api.IUserService;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    public UserDto login(UserDto userDto) {
        UserEntity user = userRepository.findByUsername(userDto.getUsername());

        return user == null ? null : userDto;
    }

    public UserDto save(UserEntity user) {
        user.setUserRole("ROLE_USER");
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return userRepository.save(user).toDto();
    }

    public List<UserDto> getAll() {
        return toDto(userRepository.findAll());
    }

    List<UserDto> toDto(Iterable<UserEntity> users){
        List<UserDto> userDtos = new ArrayList<>();
        for (UserEntity user : users)
            userDtos.add(user.toDto());
        return userDtos;
    }



}
