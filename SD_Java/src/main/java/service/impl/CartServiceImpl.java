package service.impl;

import dto.CartDto;
import model.CartEntity;
import model.DrugEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.CartRepository;
import repository.DrugRepository;
import service.api.ICartService;
@Service
public class CartServiceImpl implements ICartService {
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private DrugRepository drugRepository;

    @Override
    public CartEntity postCart(CartDto cartDto) {
        CartEntity cart = new CartEntity();
        cart.fromDto(cartDto);
        int quantity;
        for (DrugEntity d : cart.getDrugs()) {
            DrugEntity drugEntity = drugRepository.findByName(d.getName());
            quantity = drugEntity.getQuantity();
            quantity--;
            drugEntity.setQuantity(quantity);
            drugRepository.save(d);
        }

        cartRepository.save(cart);
        return cart;
    }
}
