package service.impl;

import dto.DoctorDto;
import model.DoctorEntity;
import model.HospitalEntity;
import model.SectorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DoctorRepository;
import repository.HospitalRepository;
import repository.SectorRepository;
import service.api.IDoctorService;

import java.util.ArrayList;
import java.util.List;


@Service
public class DoctorService implements IDoctorService {
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private SectorRepository sectorRepository;
    @Autowired
    private HospitalRepository hospitalRepository;

    public DoctorDto getDoctor(Long id) {
        DoctorEntity doctorEntity = doctorRepository.findOne(id);
        return doctorEntity != null ? doctorEntity.toDto() : null;
    }

    public List<DoctorDto> getDoctors(String hospital, String city) {
        List<DoctorEntity> doctors = doctorRepository.findAllByHospitalNameAndHospitalCity(hospital, city);
        return doctors != null ? convertEntitisToDtos(doctors) : null;
    }

    public List<DoctorDto> getDoctorsForAppointment(String hospital, String city, String sector) {
        List<DoctorEntity> doctors = doctorRepository.findAllByHospitalNameAndHospitalCityAndSectorEntityName(hospital, city, sector);
        return doctors != null ? convertEntitisToDtos(doctors) : null;
    }

    public List<DoctorDto> getAllDoctors() {
        List<DoctorEntity> doctors = (List<DoctorEntity>) doctorRepository.findAll();
        List<DoctorDto> dtos = convertEntitisToDtos(doctors);

        return dtos.size() > 0 ? dtos : null;
    }

    private List<DoctorDto> convertEntitisToDtos(List<DoctorEntity> doctors) {
        List<DoctorDto> dtos = new ArrayList<>();
        doctors.forEach(x -> dtos.add(x.toDto()));
        return dtos;
    }

    public DoctorDto addDoctor(DoctorDto dto) {

        if (doctorRepository.findByName(dto.getName()) != null) return null;

        SectorEntity sectorEntity = sectorRepository.findSectorByName(dto.getSector().getName());
        HospitalEntity hospitalEntity = hospitalRepository.findByName(dto.getHospital().getName()).iterator().next();

        DoctorEntity entity = new DoctorEntity();
        entity.fromDto(dto);
        entity.setSectorEntity(sectorEntity);
        entity.setHospital(hospitalEntity);
        doctorRepository.save(entity);


        return dto;
    }

    public DoctorDto updateDoctor(String name, DoctorDto dto) {
        if (null != doctorRepository.findByName(name)) {
            DoctorEntity entity = doctorRepository.findByName(name);
            entity.fromDto(dto);

            SectorEntity sectorEntity = sectorRepository.findSectorByName(dto.getSector().getName());
            HospitalEntity hospitalEntity = hospitalRepository.findByName(dto.getHospital().getName()).iterator().next();
            entity.setSectorEntity(sectorEntity);
            entity.setHospital(hospitalEntity);
            doctorRepository.save(entity);
            return entity.toDto();
        }
        return null;
    }

    public void deleteDoctor(String name) {
        doctorRepository.delete(doctorRepository.findByName(name));
    }

    public boolean canDelete(String name) {
        return doctorRepository.findByName(name) != null;
    }


}
